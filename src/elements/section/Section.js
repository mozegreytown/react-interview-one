import "./Section.css";
import {Creator} from "../../containers/Creator";
import {SectionContainer} from "../../containers/SectionContainer";

export const Section = Creator(SectionContainer, (props, child) => {
    // here you can edit props then set element before rendering or do nothing or regular react stuff
    child.setElement(props)

    // don't forget return
    return child.render();
});