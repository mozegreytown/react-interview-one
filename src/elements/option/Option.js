import "./Option.css";
import {OptionContainer} from "../../containers/OptionContainer";
import {Creator} from "../../containers/Creator";

export const Option = Creator(OptionContainer);