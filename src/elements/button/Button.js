import "./Button.css";
import {ButtonContainer} from "../../containers/ButtonContainer";
import {Creator} from "../../containers/Creator";
export const Button = Creator(ButtonContainer, (props, child) => {
    // here you can edit props then set element before rendering or do nothing or regular react stuff
    child.setElement(props)
    // don't forget return
    return child.render();
});