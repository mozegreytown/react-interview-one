import "./Icon.css";
import {SpanContainer} from "../../containers/SpanContainer";
import {Creator} from "../../containers/Creator";

export const Icon = Creator(SpanContainer, (props, child) => {
    child.setElement({
        ...props,
        className: props.className ? `material-icons ${props.className} icon` : "material-icons icon"
    })

    return child.render();
})