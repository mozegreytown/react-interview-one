import "./ListItem.css";
import {ListItemContainer} from "../../containers/ListItemContainer";
import {Creator} from "../../containers/Creator";

export const ListItem = Creator(ListItemContainer);