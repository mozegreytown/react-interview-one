import "./List.css";
import {Creator} from "../../containers/Creator";
import {ListContainer} from "../../containers/ListContainer";
export const List = Creator(ListContainer);