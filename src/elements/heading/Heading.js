import "./Heading.css";
import {Creator} from "../../containers/Creator";
import {HeadingContainer} from "../../containers/HeadingContainer";
import {TEST_KEY} from "../../utils/contants";
export const Heading = Creator(HeadingContainer, (props, child) => {
    props = props || {};
    const type = typeof props.type === "number" ? `h${props.type}` : typeof props.type === "string" ? props.type : 'h1';

    child.setElement({
        ...props,
        type: type,
        [TEST_KEY]: type
    })
    return child.render();
});