import "./Select.css";
import {Creator} from "../../containers/Creator";
import {SelectContainer} from "../../containers/SelectContainer";
export const Select = Creator(SelectContainer);