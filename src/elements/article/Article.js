import {ArticleContainer} from "../../containers/ArticleContainer";
import {Creator} from "../../containers/Creator";
import "./Article.css";

export const Article = Creator(ArticleContainer);