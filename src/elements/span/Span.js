import "./Span.css";
import {SpanContainer} from "../../containers/SpanContainer";
import {Creator} from "../../containers/Creator";

export const Span = Creator(SpanContainer);
