import {Select} from "../../elements/select/Select";
import {Option} from "../../elements/option/Option";
import "./OptionSelect.css"
import {Article} from "../../elements/article/Article";
export const OptionSelector = (props) => {
    const {options, onChange, selectedValue} = props;

    return (
        <Article className={"select__container"}>
            <Select {...props} value={selectedValue} onChange={onChange}>
                {options.map((value, index) =><Option
                    key={index}
                >{value}</Option>)}
            </Select>
        </Article>
    )
}