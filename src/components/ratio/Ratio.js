import "./Ratio.css"
import {Creator} from "../../containers/Creator";
import {ArticleContainer} from "../../containers/ArticleContainer";
export const Ratio = Creator(ArticleContainer, (props, child) => {
    child.setElement({
        ...props,
        className: props.className ? `${props.className} ratio` : "ratio"
    });
    return child.render();
});