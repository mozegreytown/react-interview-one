import {Section} from "../../elements/section/Section";
import {Icon} from "../../elements/icon/Icon";
import {Article} from "../../elements/article/Article";
import "./Pagination.css";
import {Button} from "../../elements/button/Button";
import {Span} from "../../elements/span/Span";

export const Pagination = () => {
    return(
        <Section className={"pagination"}>
            <Icon className={"pagination__icon"}>arrow_back_ios_new</Icon>
            <Article className={"pagination__numbers"}>
                {[1, 2, 3, 4].map((item, index) => <Button className={"pagination__numbers-item"} key={index}>{item}</Button>)}
            </Article>
            <Icon className={"pagination__icon"}>arrow_forward_ios</Icon>
        </Section>
    )
}