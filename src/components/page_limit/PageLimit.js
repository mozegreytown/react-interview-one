import {Section} from "../../elements/section/Section";
import {OptionSelector} from "../select/OptionSelector";
import {Icon} from "../../elements/icon/Icon";
import "./PageLimit.css";

export const PageLimit = (props) => {
    const {onChange, selectedRange} = props;
    return <Section className={"page-limit"}>
        <OptionSelector
            options={[4, 8, 12]}
            onChange={onChange}
            selectedValue={selectedRange}
        />
        <Icon className={"page-limit__icon"}>grid_view</Icon>
    </Section>
}