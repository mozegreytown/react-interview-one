import "./Grid.css"
import {Creator} from "../../containers/Creator";
import {SectionContainer} from "../../containers/SectionContainer";
export const Grid = Creator(SectionContainer, (props, child) => {
    child.setElement({
        ...props,
        className: props.className ? `${props.className} grid` :"grid"
    });
    return child.render();
});