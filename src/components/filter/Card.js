import "./Card.css"
import {Creator} from "../../containers/Creator";
import {SectionContainer} from "../../containers/SectionContainer";
export const Card = Creator(SectionContainer, (props, child) => {
    child.setElement({
        ...props,
        className: props.className ? `${props.className} card` : "card"
    });
    return child.render();
});