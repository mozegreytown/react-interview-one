import {OptionSelector} from "../select/OptionSelector";
import {Section} from "../../elements/section/Section";

export const CategoryFilter = (props) => {
    const {filteredCats, onChange} = props;

    return (
       <Section>
           <OptionSelector
               className={"movie__select__category"}
               options={filteredCats}
               onChange={(e) => onChange(e.target.value)}
           />
       </Section>
    )
}