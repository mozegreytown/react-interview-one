import {Section} from "../../elements/section/Section";
import {Span} from "../../elements/span/Span";
import React from "react";
import {Article} from "../../elements/article/Article";
import "./Navbar.css";

export const Navbar = () => {
    return (
        <Section className={"nav"}>
            <Article>
                THE MOVIE LIST
            </Article>
            <Article className={"nav__chart__icon"}>
                <Span className={"material-icons nav__chart-icon"}>bar_chart</Span>
            </Article>
        </Section>
    )
};