import {Card} from "../card/Card";
import {Grid} from "../grid/Grid";
import {Section} from "../../elements/section/Section";
import {Heading} from "../../elements/heading/Heading";
import {useEffect, useState} from "react";
import {Article} from "../../elements/article/Article";
import {Icon} from "../../elements/icon/Icon";
import {CategoryFilter} from "../filter/CategoryFilter";
import {Pagination} from "../pagination/Pagination";
import {useDispatch, useSelector} from "react-redux";
import {PageLimit} from "../page_limit/PageLimit";
import "./Movie.css";
import {createRange} from "../../utils/helper";
import {Ratio} from "../ratio/Ratio";
import {Button} from "../../elements/button/Button";
import {MovieSlice} from "../../features/movie_slice";

export const Movie = () => {
    const rangeLimit = 12;
    const movies = useSelector((state) => state.movies);
    const {setMovies} = MovieSlice.actions;
    const dispatch = useDispatch();
    const [filteredCats, setFilteredCats] = useState([]);
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [selectedCat, setSelectedCat] = useState(filteredCats[selectedIndex]);
    const [groups, setGroups] = useState({});
    const [categoryMovies, setCategoryMovies] = useState([]);
    const [filterToggle, setFilterToggle] = useState(false);
    const [ranges, setRanges] = useState(createRange(rangeLimit));
    const [selectedRange, setSelectedRange] = useState(rangeLimit);
    const [list, setList] = useState([]);
    const [like, setLike] = useState({});
    const [hideFilter, setHideFilter] = useState(false);

    useEffect(() => {
       handleSetGroups();
    }, [filteredCats]);

    useEffect(() => {
        handleSelectedCat();
    }, [selectedIndex]);

    useEffect(() => {
        handleCategoryMovies();
    }, [selectedCat]);

    useEffect(() => {
        setFilteredCats(filterCats(movies));

        if (filterToggle) {
            setSelectedCat(filteredCats[selectedIndex]);
        }

        setList(filterToggle && selectedCat ? categoryMovies : movies);
    }, [movies, rangeLimit, filterToggle, categoryMovies])

    function handleRanges(limit) {
        let _ranges = [];
        setSelectedRange(limit);
        movies.map((c, i) => i < limit && _ranges.push(i));
        setRanges(_ranges);
    }
    function handleSetGroups() {
        let _groups = {};
        if (!filteredCats) return;

        for (let category of filteredCats) {
            _groups[category] = movies.filter(movie => movie.category === category);
        }

        setGroups(_groups);
    }
    function handleSelectedCat() {
        setSelectedCat(filteredCats[selectedIndex]);
    }
    function handleCategoryMovies() {
        setCategoryMovies(groups[selectedCat]);
    }
    const handleSelectChange = (value) => {
        console.log(value);
        if (!filteredCats) return;
        let index = filteredCats.indexOf(value);

        if (index !== -1) {
            setSelectedIndex(index);
        }
    }
    function filterCats (movies) {
        if (!movies) return;

        let categories = movies.map(m => m.category);
        let filteredCategories = [];

        for (const category of categories) {
            if (!filteredCategories.find(c => c === category)) {
                filteredCategories.push(category);
            }
        }
        return filteredCategories;
    }
    const handleFilterToggle = () => {
        setFilterToggle(!filterToggle);
    }
    const handleRemoveItem = (movie) => {
        if (filterToggle) {
            return;
        }
        let _list = list;

        _list = _list.filter(item => item.id !== movie.id);
        let _new_cats = [];

        _list.forEach(li => !_new_cats.find(nc => nc === li.category) && _new_cats.push(li.category));

        if (_new_cats.length === 1) {
            setHideFilter(true);
        }

        setList(_list);
        setFilteredCats(_new_cats);
        dispatch(setMovies(_list));
    }

    const handleLike = (id) => {
        if (like[id] === undefined) like[id] = false;

        let _like = {
            ...like
        };

        _like[id] = !_like[id];

        setLike(_like);

    }

    const reload = () => {
      window.location.reload();
    }

    return(
        <Section>
            {/* CATEGORIES */}
            {<Grid className={"movie__category-list"}>
                {filteredCats && filteredCats.map((item, index) =>
                    <Button onClick={() => handleSelectChange(item)} className={"movie__category-list__item"}
                            key={index}>
                        {item}
                    </Button>
                )}
            </Grid>}
            {/* TOGGLE FILTER */}
            {movies.length === 0 && hideFilter && <Article onClick={() => reload()} className={"movie__category-and-toggle"}>
                <Icon>refresh</Icon>
            </Article>}
            {
                !hideFilter && <Section className={"movie__sub__menu"}>
                    <Article className={"movie__category-and-toggle"}>
                        <Section className={"movie__category-and-toggle"}>
                            <Icon className={"toggle"} onClick={handleFilterToggle}>{filterToggle ? "toggle_on" : "toggle_off"}</Icon>
                            <Icon className={"movie__category-filter-icon"}>filter_list_alt</Icon>
                        </Section>
                        {/* SELECT LIMIT */}
                        {
                            movies.length > 0 && <PageLimit
                                onChange={(e) => handleRanges(e.target.value)}
                                selectedRange={selectedRange}
                            />
                        }
                        {/* SELECT CATEGORY */}
                        { filterToggle && !hideFilter && <Article className={"movie__category-and-filter"}>
                            <CategoryFilter
                                filteredCats={filteredCats}
                                movies={list}
                                onChange={handleSelectChange}
                            />
                        </Article>
                        }
                    </Article>
                </Section>
            }
             <Grid>
                {list && list.filter((i, index) => ranges.indexOf(index) !== -1).map((item, index) =>
                    <Card key={index}>
                        <Heading className={"card__title"} type={2}>{item.title}</Heading>
                        <Heading className={"card__category"} type={2}>{item.category}</Heading>
                        <Section className={"movie__card__like-and-toggle"}>
                            <Article className={"movie__card__like"}><Icon>{like[item.id] ? "favorite" : "favorite_border"}</Icon></Article>
                            <Article><Icon className={"toggle"} onClick={() => handleLike(item.id)}>{like[item.id] ? "toggle_on" : "toggle_off"}</Icon>
                            </Article>
                        </Section>
                        <Ratio>
                            <Article className={"ration__text"}>33 % Likes</Article>
                            <Article className={"ration__progress-bar"} >
                                <Article className={"ration__progress-bar__progression"}></Article>
                            </Article>
                        </Ratio>
                        {!filterToggle && <Article onClick={() => handleRemoveItem(item)} className={"movie__card__bottom"}>
                            <Icon className={"movie__card__icon-remove"}>remove_circle</Icon>
                        </Article>}
                    </Card>
                )}
            </Grid>
            {movies.length > 0 && <Pagination/>}
            <Section className={"bottom-margin"} />
        </Section>
    )
}