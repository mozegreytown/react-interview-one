import React from "react";
import {Creator} from "../containers/Creator";
import {SectionContainer} from "../containers/SectionContainer";
import {Section} from "../elements/section/Section";
import {List} from "../elements/list/List";
import {ListItem} from "../elements/listItem/ListItem";
import {Navbar} from "../components/nav/Navbar";
import "./PageView.css";

export const PageView = Creator(SectionContainer, (props, child) => {
    child.setElement({
        ...props,
        children: <Section>
           <Navbar/>
            {/*CONTENT*/}
            <Section className={"view__content"}>
                {props.children}
            </Section>
            {/*FOOTER*/}
            {/*<Section className={"view__footer"}>
                <List className={"footer__items"}>
                    <ListItem>Admin</ListItem>
                    <ListItem>Access</ListItem>
                    <ListItem>Profile</ListItem>
                    <ListItem>About</ListItem>
                </List>
            </Section>*/}
       </Section>
    });

   return child.render();
});