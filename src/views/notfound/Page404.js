import {PageView} from "../PageView";
import {Heading} from "../../elements/heading/Heading";
export const Page404 = () => {
    return (
        <PageView>
            <Heading type={2}>
                Sorry ! Page Not Found. 404
            </Heading>
        </PageView>
    )
}