import {PageView} from "../PageView";
import {Movie} from "../../components/movie/Movie";
import React from "react";
import "./Home.css";

export const Home = () => {

    return (
        <PageView>
            <Movie />
        </PageView>
    )
}