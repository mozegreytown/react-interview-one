import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class NavContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "nav"
    }
}