import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class ButtonContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "button"
    }
}