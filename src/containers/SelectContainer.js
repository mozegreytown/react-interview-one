import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class SelectContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "select"
    }
}