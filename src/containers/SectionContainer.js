import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class SectionContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "section"
    }
}