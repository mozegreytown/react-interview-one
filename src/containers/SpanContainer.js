import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class SpanContainer extends HTMLHandler {

    className() {
        return this.tagName();
    }

    tagName() {
        return "span"
    }
}