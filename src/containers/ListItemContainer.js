import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class ListItemContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "ul"
    }
}