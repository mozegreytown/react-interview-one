/**
 * @param parentHandler {HTMLHandler}
 * @param needElement {boolean}
 * @param initializer
 *
 * */
import {HTMLHandler} from "../patterns/template/HTMLHandler";

export function Creator(
    parentHandler = null,
    // can be a string or a component
    initializer = null,
    needContainer = false
) {
    return (props) => {
        props = props || {};

        const container = parentHandler && (new class T extends parentHandler {});
        if (typeof initializer == "string") container.setElement({...props, type: initializer })
        else container.setElement({...props});

        //run hooks or logic for each component
        if (initializer && typeof initializer === "function") return initializer(props, container);

        return needContainer ? needContainer : container._element;
    }
}