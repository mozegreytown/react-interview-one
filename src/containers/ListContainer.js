import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class ListContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "ul"
    }
}