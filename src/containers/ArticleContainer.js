import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class ArticleContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "article"
    }
}