import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class OptionContainer extends HTMLHandler {
    className() {
        return this.tagName();
    }

    tagName() {
        return "option"
    }
}