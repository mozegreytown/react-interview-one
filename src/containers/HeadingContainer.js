import {HTMLHandler} from "../patterns/template/HTMLHandler";

export class HeadingContainer extends HTMLHandler {
    className() {
        return "heading";
    }

    tagName() {
        return "h1"
    }
}