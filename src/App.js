import "./App.css"
import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Home} from "./views/home/Home";
import {Page404} from "./views/notfound/Page404";
import {UseMovies} from "./app/hooks/UseMovies";

export const App = () => {
    UseMovies();
    return (
        <BrowserRouter basename={"/"}>
            <Routes>
                <Route path={"/"} exact element={<Home />} />
                <Route path={"*"} element={<Page404 />} />
            </Routes>
        </BrowserRouter>
    )
}