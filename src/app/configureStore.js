import {configureStore} from "@reduxjs/toolkit";
import {MovieSlice} from "../features/movie_slice";

export const store = configureStore({
    reducer: {
        movies: MovieSlice.reducer
    }
})