import {useDispatch, useSelector} from "react-redux";
import {MovieSlice} from "../../features/movie_slice";
import {movies$} from "../movies";
import {useEffect} from "react";

export const UseMovies = () => {
    const {setMovies} = MovieSlice.actions;
    const dispatch = useDispatch();

    useEffect(() => {
       getMovies();
    }, [])

    async function getMovies() {
        let data = await movies$;
        dispatch(setMovies(data));
    }
}