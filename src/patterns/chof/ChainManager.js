export class ChainManager {
    /**@param handler {ChainHandler}*/
    constructor(handler) {
        this._handler = handler;
    }

    handle(option) {
        if (this._handler) this._handler.handle(option);
    }
}