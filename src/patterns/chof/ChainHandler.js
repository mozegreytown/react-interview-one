import {ChainInterface} from "./ChainInterface";


/**@abstract*/
export class ChainHandler extends ChainInterface {
    constructor(next) {
        super();
        this._next = next;
    }
    handle(option) {
        if(this.doHandle(option)) return;
        if (this._next) this._next.handle(option);
    }
}