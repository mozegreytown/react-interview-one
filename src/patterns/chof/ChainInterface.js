/**@interface*/
export class ChainInterface {
    /**
     * @abstract
     * @param option {Object}
     *
     * */
    handle(option){}
    /**
     * @abstract
     * @param option {Object}
     * @return boolean
     * */
    doHandle(option) {}
}