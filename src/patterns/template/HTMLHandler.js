
import {HTMLHandlerInterface} from "./HTMLHandlerInterface";
import React from "react";

/**@abstract*/
export class HTMLHandler extends HTMLHandlerInterface {

    setAttribute(props) {
        // chain
    }

    setElement(props) {
        let _props = {...props} || {};
        if (_props.hasOwnProperty("selectedValue")) {
            delete _props.selectedValue;
        }

        this._element = React.createElement(props.type && typeof props.type !== "number" ? props.type : this.tagName(), {
            ..._props,
            component_type: "function",
            className: _props.className ? `${_props.className} ${this.className()}` : this.className(),
        });
    }

    render(){
        return this._element;
    }
}