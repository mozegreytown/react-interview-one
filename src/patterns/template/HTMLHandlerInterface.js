/**@interface*/
export class HTMLHandlerInterface {
    /**@abstract*/
    setElement(props) {}
    /**@abstract*/
    setAttribute(props) {}
    /**@abstract*/
    tagName() {}
    /**@abstract*/
    className() {}
    /**@abstract*/
    render() {}
}